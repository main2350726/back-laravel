
FROM bitnami/laravel:latest


RUN apt-get update && apt-get install -y \
    libzip-dev \
    unzip \
    git \
    curl


COPY --from=composer:latest /usr/bin/composer /usr/bin/composer


WORKDIR /var/www


COPY . .


RUN chown -R www-data:www-data /var/www


RUN composer install


EXPOSE 8000
