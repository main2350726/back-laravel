<?php

namespace App\DataObjects;

use Spatie\LaravelData\Data;

class UpdateArticleData extends Data
{



    public function __construct(
        public string $title,
        public string $description,
        public ?string $image_url,
        public ?int $category_id,
        public array $tags
    ) {
    }

    public static function fromArray(array $data): static
    {
        return new static(
            $data['title'],
            $data['description'],
            $data['image_url'] ?? null,
            $data['category_id'] ?? null,
            $data['tags'] ?? []
        );
    }
}
