<?php

namespace App\DataObjects;

use Spatie\LaravelData\Data;

class UpdateUserData extends Data
{


    public function __construct(
        public string $name,
        public string $email,
        public ?string $password,
        public string $type
    ) {
    }

    public static function fromArray(array $data): static
    {
        return new static(
            $data['name'],
            $data['email'],
            $data['password'] ?? null,
            $data['type']
        );
    }

    public function toArray(): array
    {
        $data = parent::toArray();
        if ($this->password) {
            $data['password'] = bcrypt($this->password);
        }

        return $data;
    }
}
