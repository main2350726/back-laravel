<?php

namespace App\DataObjects;

use Spatie\LaravelData\Data;

class RegisterUserData extends Data
{



    public function __construct(
        public string $name,
        public string $email,
        public string $password,
        public string $type
    ) {
    }

    public function toArray(): array
    {
        $data = parent::toArray();
        $data['password'] = bcrypt($this->password);

        return $data;
    }

    public static function fromArray(array $data): static
    {
        return new static(
            $data['name'],
            $data['email'],
            $data['password'],
            $data['type'],
        );
    }
}
