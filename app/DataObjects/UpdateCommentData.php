<?php

namespace App\DataObjects;

use Spatie\LaravelData\Data;

class UpdateCommentData extends Data
{



    public function __construct(public string $content)
    {
    }

    public static function fromArray(array $data): static
    {
        return new static(

            $data['content']
        );
    }
}
