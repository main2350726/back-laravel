<?php

namespace App\DataObjects;

use Spatie\LaravelData\Data;
use App\Models\User;

class LogoutUserData extends Data
{
    public function __construct(public int $user_id)
    {
    }

    public static function fromArray(array $data): static
    {
        return new static(
            $data['user_id']
        );
    }
}

