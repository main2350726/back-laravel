<?php

namespace App\DataObjects;

use Spatie\LaravelData\Data;

class CreateCommentData extends Data
{


    public function __construct(
        public int $article_id,
        public string $content,
        public ?int $user_id
    ) {
    }

    public static function fromArray(array $data): static
    {
        return new static(
            $data['article_id'],
            $data['content'],
            $data['user_id'] ?? null
        );
    }
}
