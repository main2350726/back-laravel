<?php

namespace App\DataObjects;

use Spatie\LaravelData\Data;

class CreateArticleData extends Data
{


    public function __construct(
        public string $title,
        public string $description,
        public ?string $image_url,
        public ?int $category_id,
        public ?int $user_id,
        public array $tags
    ) {
    }

    public static function fromArray(array $data): static
    {
        return new static(
            $data['title'],
            $data['description'],
            $data['image_url'] ?? null,
            $data['category_id'] ?? null,
            $data['user_id'],
            $data['tags'] ?? []
        );
    }
}
