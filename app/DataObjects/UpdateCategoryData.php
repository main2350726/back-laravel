<?php

namespace App\DataObjects;

use Spatie\LaravelData\Data;

class UpdateCategoryData extends Data
{


    public function __construct(public string $name)
    {
    }


    public static function fromArray(array $data): static
    {
        return new static(
            $data['name']
        );
    }
}
