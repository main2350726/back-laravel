<?php
namespace App\DataObjects;

use Spatie\LaravelData\Data;

class LoginUserData extends Data
{
    public function __construct(
        public string $email,
        public string $password
    ) {}

    public static function fromArray(array $data): static
    {
        return new static (
            $data['email'],
            $data['password']
        );
    }
}


