<?php

namespace App\Http\Controllers;

use App\DataObjects\{UpdateArticleData,CreateArticleData};
use App\Models\Article;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;

class ArticleController extends Controller
{
    public function store(CreateArticleData $data): JsonResponse
    {
        $article = Article::create($data->toArray());
        $article->tags()->sync($data->tags);
        $article->load('tags');

        return response()->json($article, 201);
    }

    public function update(string $id, UpdateArticleData $data): JsonResponse
    {
        $article = Article::findOrFail($id);
        $article->update($data->toArray());
        $article->tags()->sync($data->tags);

        return response()->json($article);
    }

    public function destroy(string $id): JsonResponse
    {
        $article = Article::findOrFail($id);
        $article->delete();

        return response()->json(null, 204);
    }

    public function show(string $id): JsonResponse
    {
        $article = Article::with(['comments', 'tags'])->findOrFail($id);
        return response()->json($article);
    }

    public function index(): JsonResponse
    {
        $articles = Article::with('tags')->orderBy('created_at', 'desc')->paginate(10);
        return response()->json($articles);
    }
}



