<?php

namespace App\Http\Controllers;

use App\DataObjects\CreateTagData;
use App\DataObjects\UpdateTagData;
use App\Models\Tag;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;

class TagController extends Controller
{
    public function store(CreateTagData $data): JsonResponse
    {
        $tag = Tag::create($data->toArray());
        return response()->json($tag, 201);
    }

    public function update(string $id, UpdateTagData $data): JsonResponse
    {
        $tag = Tag::findOrFail($id);
        $tag->update($data->toArray());

        return response()->json($tag);
    }

    public function destroy(string $id): JsonResponse
    {
        $tag = Tag::findOrFail($id);
        $tag->delete();

        return response()->json(null, 204);
    }

    public function index(): JsonResponse
    {
        $tags = Tag::all();

        return response()->json($tags);
    }
}
