<?php

namespace App\Http\Controllers;

use App\DataObjects\CreateCommentData;
use App\DataObjects\UpdateCommentData;
use App\Models\Comment;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;

class CommentController extends Controller
{
    public function store(CreateCommentData $data): JsonResponse
    {
        $comment = Comment::create($data->toArray());
        return response()->json($comment, 201);
    }

    public function update(string $id, UpdateCommentData $data): JsonResponse
    {
        $comment = Comment::findOrFail($id);
        $comment->update($data->toArray());

        return response()->json($comment);
    }

    public function destroy(string $id): JsonResponse
    {
        $comment = Comment::findOrFail($id);
        $comment->delete();

        return response()->json(null, 204);
    }
}
