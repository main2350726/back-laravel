<?php

namespace App\Http\Controllers;

use App\DataObjects\LoginUserData;
use App\DataObjects\LogoutUserData;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\HasApiTokens;
use Illuminate\Routing\Controller;
use App\Models\User;
class AuthController extends Controller
{
    use HasApiTokens;

    public function login(LoginUserData $data): JsonResponse
    {
        if (Auth::attempt(['email' => $data->email, 'password' => $data->password])) {
            $user = Auth::user();
            $token = $user->createToken('Laravel Personal Access Client')->accessToken;
            return response()->json([
                'user' => $user,
                'token' => $token,
            ], 200);
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }

    public function logout(LogoutUserData $data): JsonResponse
    {
        $user = User::find($data->user_id);
        $user->tokens()->delete();
        return response()->json(['message' => 'Successfully logged out'], 200);
    }
}

