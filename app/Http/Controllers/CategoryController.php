<?php

namespace App\Http\Controllers;

use App\DataObjects\CreateCategoryData;
use App\DataObjects\UpdateCategoryData;
use App\Models\Category;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class CategoryController extends BaseController
{
    public function index(): JsonResponse
    {
        $categories = Category::all();
        return response()->json($categories);
    }

    public function store(CreateCategoryData $data): JsonResponse
    {
        $category = Category::create($data->toArray());
        return response()->json($category, 201);
    }

    public function show(int $id): JsonResponse
    {
        $category = Category::findOrFail($id);
        return response()->json($category);
    }

    public function update(int $id, UpdateCategoryData $data): JsonResponse
    {
        $category = Category::findOrFail($id);
        $category->update($data->toArray());
        return response()->json($category);
    }

    public function destroy(int $id): JsonResponse
    {
        $category = Category::findOrFail($id);
        $category->delete();
        return response()->json(null, 204);
    }
}
