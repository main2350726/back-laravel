<?php

namespace App\Http\Controllers;

use App\DataObjects\RegisterUserData;
use App\DataObjects\UpdateUserData;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;

class UserController extends Controller
{
    public function register(RegisterUserData $data): JsonResponse
    {
        $user = User::create($data->toArray());

        return response()->json($user, 201);
    }

    public function update(string $id, UpdateUserData $data): JsonResponse
    {
        $user = User::findOrFail($id);
        $user->update($data->toArray());

        return response()->json($user);
    }

    public function destroy(string $id): JsonResponse
    {
        $user = User::findOrFail($id);
        $user->delete();

        return response()->json(null, 204);
    }

    public function index(): JsonResponse
    {
        $users = User::all();

        return response()->json($users);
    }
}
