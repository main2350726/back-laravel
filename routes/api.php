<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{
    ArticleController,
    AuthController,
    CommentController,
    TagController,
    UserController,
    CategoryController
};


Route::prefix('articles')->group(function () {
    Route::get('/', [ArticleController::class, 'index']);
    Route::post('/', [ArticleController::class, 'store'])->middleware('auth:api');
    Route::get('{id}', [ArticleController::class, 'show']);
    Route::put('{id}', [ArticleController::class, 'update'])->middleware('auth:api');
    Route::delete('{id}', [ArticleController::class, 'destroy'])->middleware('auth:api');
});


Route::prefix('comments')->group(function () {
    Route::post('/', [CommentController::class, 'store']);
    Route::put('{id}', [CommentController::class, 'update'])->middleware('auth:api');
    Route::delete('{id}', [CommentController::class, 'destroy'])->middleware('auth:api');
});


Route::prefix('categories')->group(function () {
    Route::post('/', [CategoryController::class, 'store'])->middleware('auth:api');
    Route::put('{id}', [CategoryController::class, 'update'])->middleware('auth:api');
    Route::delete('{id}', [CategoryController::class, 'destroy'])->middleware('auth:api');
});


Route::prefix('tags')->group(function () {
    Route::get('/', [TagController::class, 'index']);
    Route::post('/', [TagController::class, 'store'])->middleware('auth:api');
    Route::put('{id}', [TagController::class, 'update'])->middleware('auth:api');
    Route::delete('{id}', [TagController::class, 'destroy'])->middleware('auth:api');
});


Route::prefix('users')->group(function () {
    Route::post('/register', [UserController::class, 'register']);
    Route::put('{id}', [UserController::class, 'update'])->middleware('auth:api');
    Route::delete('{id}', [UserController::class, 'destroy'])->middleware('auth:api');
    Route::get('/', [UserController::class, 'index'])->middleware('auth:api');
});


Route::post('login', [AuthController::class, 'login']);
Route::post('logout', [AuthController::class, 'logout'])->middleware('auth:api');
