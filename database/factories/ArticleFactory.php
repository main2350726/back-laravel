<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ArticleFactory extends Factory
{
    /**
     * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Article>
     */

    public function definition(): array
    {
        return [
            'title' => $this->faker->sentence,
            'image_url' => $this->faker->imageUrl,
            'description' => $this->faker->paragraph,
            'category_id' => \App\Models\Category::factory(),
            'user_id' => \App\Models\User::factory(),
        ];
    }
}
