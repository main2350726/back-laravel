<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    /**
     * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Comment>
     */

    public function definition(): array
    {
        return [
            'article_id' => \App\Models\Article::factory(),
            'user_id' => \App\Models\User::factory(),
            'content' => $this->faker->paragraph,
        ];
    }
}
