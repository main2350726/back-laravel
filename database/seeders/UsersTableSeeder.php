<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Seed the users table.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'João Silva',
                'email' => 'joao.silva@example.com',
                'password' => Hash::make('password123'),
                'type' => 'author',
            ],
            [
                'name' => 'Maria Oliveira',
                'email' => 'maria.oliveira@example.com',
                'password' => Hash::make('password123'),
                'type' => 'user',
            ],
            [
                'name' => 'Pedro Santos',
                'email' => 'pedro.santos@example.com',
                'password' => Hash::make('password123'),
                'type' => 'author',
            ],
            [
                'name' => 'Ana Costa',
                'email' => 'ana.costa@example.com',
                'password' => Hash::make('password123'),
                'type' => 'user',
            ],
            [
                'name' => 'Lucas Almeida',
                'email' => 'lucas.almeida@example.com',
                'password' => Hash::make('password123'),
                'type' => 'author',
            ],
        ]);
    }
}
