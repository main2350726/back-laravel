<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagsTableSeeder extends Seeder
{
    /**
     * Seed the tags table.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->insert([
            ['name' => 'Notícias'],
            ['name' => 'Inovação'],
            ['name' => 'Bem-estar'],
            ['name' => 'Dicas'],
            ['name' => 'Tecnologia'],
        ]);
    }
}
