<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Seed the categories table.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            ['name' => 'Tecnologia'],
            ['name' => 'Saúde'],
            ['name' => 'Educação'],
            ['name' => 'Entretenimento'],
            ['name' => 'Esportes'],
        ]);
    }
}
