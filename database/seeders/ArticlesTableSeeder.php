<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Seed the articles table.
     *
     * @return void
     */
    public function run()
    {
        DB::table('articles')->insert([
            [
                'user_id' => 1,
                'title' => 'O Futuro da Tecnologia',
                'description' => 'Um olhar sobre as tendências tecnológicas que moldarão o futuro.',
                'image_url' => 'https://via.placeholder.com/640x480.png?text=Futuro+da+Tecnologia',
                'category_id' => 1,
            ],
            [
                'user_id' => 2,
                'title' => 'Benefícios da Meditação',
                'description' => 'Como a meditação pode melhorar sua saúde mental e física.',
                'image_url' => 'https://via.placeholder.com/640x480.png?text=Benef%C3%ADcios+da+Medita%C3%A7%C3%A3o',
                'category_id' => 2,
            ],
            [
                'user_id' => 3,
                'title' => 'Educação Online: Vantagens e Desvantagens',
                'description' => 'Análise dos prós e contras do ensino à distância.',
                'image_url' => 'https://via.placeholder.com/640x480.png?text=Educa%C3%A7%C3%A3o+Online',
                'category_id' => 3,
            ],
            [
                'user_id' => 4,
                'title' => 'Os Melhores Filmes de 2024',
                'description' => 'Uma lista dos filmes que você não pode perder este ano.',
                'image_url' => 'https://via.placeholder.com/640x480.png?text=Melhores+Filmes+2024',
                'category_id' => 4,
            ],
            [
                'user_id' => 5,
                'title' => 'Como Melhorar seu Desempenho Esportivo',
                'description' => 'Dicas para aumentar sua performance em esportes.',
                'image_url' => 'https://via.placeholder.com/640x480.png?text=Desempenho+Esportivo',
                'category_id' => 5,
            ],
        ]);
    }
}
