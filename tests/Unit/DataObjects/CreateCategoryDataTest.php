<?php

namespace Tests\Unit\DataObjects;

use App\DataObjects\CreateCategoryData;
use Tests\TestCase;

class CreateCategoryDataTest extends TestCase
{
    public function test_create_category_data_is_valid(): void
    {
        $data = CreateCategoryData::fromArray([
            'name' => 'Test Category',
        ]);

        $this->assertEquals('Test Category', $data->name);
    }
}
