<?php

namespace Tests\Unit\DataObjects;

use App\DataObjects\UpdateTagData;
use Tests\TestCase;

class UpdateTagDataTest extends TestCase
{
    public function test_update_tag_data_is_valid(): void
    {
        $data = UpdateTagData::fromArray([
            'name' => 'Updated Test Tag',
        ]);

        $this->assertEquals('Updated Test Tag', $data->name);
    }
}
