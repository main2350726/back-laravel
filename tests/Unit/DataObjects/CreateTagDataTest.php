<?php

namespace Tests\Unit\DataObjects;

use App\DataObjects\CreateTagData;
use Tests\TestCase;

class CreateTagDataTest extends TestCase
{
    public function test_create_tag_data_is_valid(): void
    {
        $data = CreateTagData::fromArray([
            'name' => 'Test Tag',
        ]);

        $this->assertEquals('Test Tag', $data->name);
    }
}
