<?php

namespace Tests\Unit\DataObjects;

use App\DataObjects\UpdateCommentData;
use Tests\TestCase;

class UpdateCommentDataTest extends TestCase
{
    public function test_update_comment_data_is_valid(): void
    {
        $data = UpdateCommentData::fromArray([
            'content' => 'Updated Test Comment',
        ]);

        $this->assertEquals('Updated Test Comment', $data->content);
    }
}
