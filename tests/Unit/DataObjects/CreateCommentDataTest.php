<?php

namespace Tests\Unit\DataObjects;

use App\DataObjects\CreateCommentData;
use Tests\TestCase;

class CreateCommentDataTest extends TestCase
{
    public function test_create_comment_data_is_valid(): void
    {
        $data = CreateCommentData::fromArray([
            'content' => 'Test Comment',
            'article_id' => 1,
            'user_id' => 1,
        ]);

        $this->assertEquals('Test Comment', $data->content);
        $this->assertEquals(1, $data->article_id);
        $this->assertEquals(1, $data->user_id);
    }
}
