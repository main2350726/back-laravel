<?php

namespace Tests\Unit\DataObjects;

use App\DataObjects\CreateArticleData;
use Tests\TestCase;

class CreateArticleDataTest extends TestCase
{
    public function test_create_article_data_is_valid(): void
    {
        $data = CreateArticleData::fromArray([
            'title' => 'Test Article',
            'image_url' => 'http://example.com/image.jpg',
            'description' => 'Test Description',
            'category_id' => 1,
            'tags' => [1, 2],
        ]);

        $this->assertEquals('Test Article', $data->title);
        $this->assertEquals('http://example.com/image.jpg', $data->image_url);
        $this->assertEquals('Test Description', $data->description);
        $this->assertEquals(1, $data->category_id);
        $this->assertEquals([1, 2], $data->tags);
    }
}
