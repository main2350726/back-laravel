<?php

namespace Tests\Unit\DataObjects;

use App\DataObjects\UpdateCategoryData;
use Tests\TestCase;

class UpdateCategoryDataTest extends TestCase
{
    public function test_update_Category_data_is_valid(): void
    {
        $data = UpdateCategoryData::fromArray([
            'name' => 'Updated Test Category',
        ]);

        $this->assertEquals('Updated Test Category', $data->name);
    }
}
