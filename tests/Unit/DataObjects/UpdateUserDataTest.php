<?php

namespace Tests\Unit\DataObjects;

use App\DataObjects\UpdateUserData;
use Tests\TestCase;

class UpdateUserDataTest extends TestCase
{
    public function test_update_user_data_is_valid(): void
    {
        $data = UpdateUserData::fromArray([
            'name' => 'Updated Test User',
            'email' => 'updated_test@example.com',
            'password' => 'new_secret',
            'type' => 'editor',
        ]);

        $this->assertEquals('Updated Test User', $data->name);
        $this->assertEquals('updated_test@example.com', $data->email);
        $this->assertEquals('new_secret', $data->password);
        $this->assertEquals('editor', $data->type);
    }
}
