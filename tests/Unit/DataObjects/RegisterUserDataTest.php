<?php

namespace Tests\Unit\DataObjects;

use App\DataObjects\RegisterUserData;
use Tests\TestCase;

class RegisterUserDataTest extends TestCase
{
    public function test_register_user_data_is_valid(): void
    {
        $data = RegisterUserData::fromArray([
            'name' => 'Test User',
            'email' => 'test@example.com',
            'password' => 'secret',
            'type' => 'author',
        ]);

        $this->assertEquals('Test User', $data->name);
        $this->assertEquals('test@example.com', $data->email);
        $this->assertEquals('secret', $data->password);
        $this->assertEquals('author', $data->type);
    }
}
