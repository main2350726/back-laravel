<?php

namespace Tests\Unit\DataObjects;

use App\DataObjects\UpdateArticleData;
use Tests\TestCase;

class UpdateArticleDataTest extends TestCase
{
    public function test_update_article_data_is_valid(): void
    {
        $data = UpdateArticleData::fromArray([
            'title' => 'Updated Test Article',
            'image_url' => 'http://example.com/updated-image.jpg',
            'description' => 'Updated Test Description',
            'category_id' => 2,
            'tags' => ['updated_tag1', 'updated_tag2'],
        ]);

        $this->assertEquals('Updated Test Article', $data->title);
        $this->assertEquals('http://example.com/updated-image.jpg', $data->image_url);
        $this->assertEquals('Updated Test Description', $data->description);
        $this->assertEquals(2, $data->category_id);
        $this->assertEquals(['updated_tag1', 'updated_tag2'], $data->tags);
    }
}
