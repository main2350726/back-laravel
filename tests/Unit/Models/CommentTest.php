<?php


namespace Tests\Unit\Models;

use App\Models\Comment;
use Tests\TestCase;

class CommentTest extends TestCase
{
    public function test_comment_has_expected_attributes(): void
    {
        $article = new Comment([
            'content' => 'This is a test content',
            'author' => 'Test Author',
            'article_id' => 1,
            'user_id' => 1,
        ]);

        $this->assertEquals('This is a test content', $article->content);
        $this->assertEquals('Test Author', $article->author);
        $this->assertEquals(1, $article->article_id);
        $this->assertEquals(1, $article->user_id);
    }
}
