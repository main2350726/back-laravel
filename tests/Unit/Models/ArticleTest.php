<?php


namespace Tests\Unit\Models;

use App\Models\Article;
use Tests\TestCase;

class ArticleTest extends TestCase
{
    public function test_article_has_expected_attributes(): void
    {
        $article = new Article([
            'title' => 'Test Article',
            'description' => 'This is a test article',
            'image_url' => 'http://example.com/image.jpg',
            'category_id' => 1,
        ]);

        $this->assertEquals('Test Article', $article->title);
        $this->assertEquals('This is a test article', $article->description);
        $this->assertEquals('http://example.com/image.jpg', $article->image_url);
        $this->assertEquals(1, $article->category_id);
    }
}
