<?php


namespace Tests\Unit\Models;

use App\Models\Category;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    public function test_category_has_expected_attributes(): void
    {
        $article = new Category([
            'name' => 'Test Category',
        ]);

        $this->assertEquals('Test Category', $article->name);
    }
}
