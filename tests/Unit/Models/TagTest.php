<?php


namespace Tests\Unit\Models;

use App\Models\Tag;
use Tests\TestCase;

class TagTest extends TestCase
{
    public function test_tag_has_expected_attributes(): void
    {
        $article = new Tag([
            'name' => 'This is a tag test',
        ]);

        $this->assertEquals('This is a tag test', $article->name);
    }
}
