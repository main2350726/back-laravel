<?php

namespace Tests\Feature;

use App\Models\{Article, User, Category, Tag};
use Laravel\Passport\Passport;
use Tests\Feature\BaseFeatureTest;

class ArticleControllerTest extends BaseFeatureTest
{

    public function test_index(): void
    {
        Article::factory()->count(3)->create();
        $response = $this->getJson('/api/articles');
        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id', 'title', 'description', 'image_url', 'category_id', 'user_id', 'created_at', 'updated_at', 'tags'
                    ]
                ]
            ]);;
    }

    public function test_store(): void
    {

        $user = User::factory()->create();
        Passport::actingAs($user);

        $category = Category::factory()->create();
        $tags = Tag::factory()->count(2)->create();

        $data = [
            'title' => 'New Article',
            'description' => 'Article Description',
            'image_url' => 'http://example.com/image.jpg',
            'category_id' => $category->id,
            'user_id' => $user->id,
            'tags' => $tags->pluck('id')->toArray()
        ];

        $response = $this->postJson('/api/articles', $data);
        $response->assertStatus(201);

        $this->assertDatabaseHas('articles', [
            'title' => 'New Article',
            'description' => 'Article Description',
            'image_url' => 'http://example.com/image.jpg',
            'category_id' => $category->id,
            'user_id' => $user->id
        ]);

        $articleId = $response->json('id');
        foreach ($tags as $tag) {
            $this->assertDatabaseHas('article_tag', [
                'article_id' => $articleId,
                'tag_id' => $tag->id
            ]);
        }
        $response->assertJsonFragment(['title' => 'New Article']);
        $response->assertJsonFragment(['description' => 'Article Description']);
        $response->assertJsonFragment(['image_url' => 'http://example.com/image.jpg']);
        $response->assertJsonFragment(['category_id' => $category->id]);
        $response->assertJsonFragment(['user_id' => $user->id]);

        $responseData = $response->json();
        $this->assertArrayHasKey('tags', $responseData);
        foreach ($tags as $tag) {
            $this->assertTrue(in_array(['id' => $tag->id, 'name' => $tag->name], array_map(function ($t) {
                return ['id' => $t['id'], 'name' => $t['name']];
            }, $responseData['tags'])));
        }
    }


    public function test_show(): void
    {
        $article = Article::factory()->create();

        $response = $this->getJson("/api/articles/{$article->id}");

        $response->assertStatus(200)
            ->assertJsonFragment(['title' => $article->title]);
    }

    public function test_update(): void
    {

        $user = User::factory()->create();
        $article = Article::factory()->create();
        Passport::actingAs($user);

        $category = Category::factory()->create();
        $tags = Tag::factory()->count(2)->create();

        $data = [
            'title' => 'Updated Title',
            'description' => 'Article Description',
            'image_url' => 'http://example.com/image.jpg',
            'category_id' => $category->id,
            'user_id' => $user->id,
            'tags' => $tags->pluck('id')->toArray()
        ];

        $response = $this->putJson("/api/articles/{$article->id}", $data);

        $response->assertStatus(200)
            ->assertJsonFragment(['title' => 'Updated Title']);
    }

    public function test_destroy(): void
    {
        Passport::actingAs(User::factory()->create());

        $article = Article::factory()->create();

        $response = $this->deleteJson("/api/articles/{$article->id}");

        $response->assertStatus(204);
        $this->assertDatabaseMissing('articles', ['id' => $article->id]);
    }
}
