<?php

namespace Tests\Feature;


use Laravel\Passport\Passport;
use Tests\Feature\BaseFeatureTest;

class AuthControllerTest extends BaseFeatureTest
{


    public function test_login(): void
    {
        $login = $this->login();
        $login->response->assertStatus(200)
            ->assertJsonStructure([
                'user' => [
                    'id',
                    'name',
                    'email',
                    'created_at',
                    'updated_at',
                ],
                'token',
            ]);

        $this->assertAuthenticatedAs($login->user);
    }

    public function test_login_invalid_credentials(): void
    {
        $login = $this->login('wrong_password');
        $login->response->assertStatus(401)
            ->assertJsonFragment(['error' => 'Unauthorized']);
    }


    public function test_logout(): void
    {
        $login = $this->login();
        $user = $login->user;
        Passport::actingAs($user);

        $response = $this->postJson('/api/logout', ["user_id" => 35]);
        $response->assertStatus(200)
            ->assertJsonFragment(['message' => 'Successfully logged out']);
    }

}
