<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\User;
use Tests\Feature\BaseFeatureTest;

class CategoryControllerTest extends BaseFeatureTest
{

    public function test_index(): void
    {
        Category::factory()->count(3)->create();

        $response = $this->getJson('/api/categories');

        $response->assertStatus(200)
            ->assertJsonCount(3);
    }

    public function test_store(): void
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user, 'api')->postJson('/api/categories', [
            'name' => 'New Category',
        ]);

        $response->assertStatus(201)
            ->assertJsonPath('name', 'New Category');
    }

    public function test_show(): void
    {
        $category = Category::factory()->create();

        $response = $this->getJson("/api/categories/{$category->id}");

        $response->assertStatus(200)
            ->assertJsonPath('name', $category->name);
    }

    public function test_update(): void
    {
        $user = User::factory()->create();
        $category = Category::factory()->create();

        $response = $this->actingAs($user, 'api')->putJson("/api/categories/{$category->id}", [
            'name' => 'Updated Category',
        ]);

        $response->assertStatus(200)
            ->assertJsonPath('name', 'Updated Category');
    }

    public function test_destroy(): void
    {
        $user = User::factory()->create();
        $category = Category::factory()->create();

        $response = $this->actingAs($user, 'api')->deleteJson("/api/categories/{$category->id}");

        $response->assertStatus(204);
    }
}
