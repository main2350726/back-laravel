<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class BaseFeatureTest extends TestCase
{
	use DatabaseTransactions;


	protected function login(string $password = "password"): object
	{
        $user = User::factory()->create(['password' => bcrypt('password')]);
        $response = $this->postJson('/api/login', [
            'email' => $user->email,
            'password' => $password,
        ]);
        return (object) ['user' => $user, 'response'=>$response];
	}


}
