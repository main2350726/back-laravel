<?php

namespace Tests\Feature;

use App\Models\{Comment, User, Article};
use Laravel\Passport\Passport;
use Tests\Feature\BaseFeatureTest;

class CommentControllerTest extends BaseFeatureTest
{

    public function test_store(): void
    {
        $user = User::factory()->create();
        Passport::actingAs($user);

        $article = Article::factory()->create();

        $response = $this->postJson('/api/comments', [
            'article_id' => $article->id,
            'content' => 'New Comment'
        ]);

        $response->assertStatus(201)
            ->assertJsonFragment(['content' => 'New Comment']);
        $this->assertDatabaseHas('comments', ['content' => 'New Comment']);
    }

    public function test_update(): void
    {
        $user = User::factory()->create();
        Passport::actingAs($user);

        $comment = Comment::factory()->create();

        $response = $this->putJson("/api/comments/{$comment->id}", [
            'content' => 'Updated Comment'
        ]);

        $response->assertStatus(200)
            ->assertJsonFragment(['content' => 'Updated Comment']);
        $this->assertDatabaseHas('comments', ['content' => 'Updated Comment']);
    }

    public function test_destroy(): void
    {
        $user = User::factory()->create();
        Passport::actingAs($user);

        $comment = Comment::factory()->create();

        $response = $this->deleteJson("/api/comments/{$comment->id}");

        $response->assertStatus(204);
        $this->assertDatabaseMissing('comments', ['id' => $comment->id]);
    }
}
