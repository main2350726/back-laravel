<?php


namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\Feature\BaseFeatureTest;

class UserControllerTest extends BaseFeatureTest
{

    public function test_register(): void
    {
        $response = $this->postJson('/api/users/register', [
            'name' => 'New User',
            'email' => 'newuser@example.com',
            'password' => 'password',
            'type' => 'author'
        ]);

        $response->assertStatus(201)
            ->assertJsonFragment(['name' => 'New User'])
            ->assertJsonFragment(['email' => 'newuser@example.com']);
        $this->assertDatabaseHas('users', ['email' => 'newuser@example.com']);
    }

    public function test_update(): void
    {
        $user = User::factory()->create();
        Passport::actingAs($user);

        $response = $this->putJson("/api/users/{$user->id}", [
            'name' => 'Updated User'
        ]);

        $response->assertStatus(200)
            ->assertJsonFragment(['name' => 'Updated User']);
        $this->assertDatabaseHas('users', ['name' => 'Updated User']);
    }

    public function test_destroy(): void
    {
        $user = User::factory()->create();
        Passport::actingAs($user);

        $response = $this->deleteJson("/api/users/{$user->id}");

        $response->assertStatus(204);
        $this->assertDatabaseMissing('users', ['id' => $user->id]);
    }

    public function test_index(): void
    {
        User::factory()->count(3)->create();

        Passport::actingAs(User::factory()->create());

        $response = $this->getJson('/api/users');

        $response->assertStatus(200)
            ->assertJsonStructure([
                '*' => ['id', 'name', 'email', 'created_at', 'updated_at']
            ]);
    }
}
