<?php

namespace Tests\Feature;

use App\Models\{Tag, User};
use Laravel\Passport\Passport;
use Tests\Feature\BaseFeatureTest;

class TagControllerTest extends BaseFeatureTest
{

    public function test_index(): void
    {
        Tag::factory()->count(3)->create();

        $response = $this->getJson('/api/tags');

        $response->assertStatus(200)
            ->assertJsonStructure([
                '*' => ['id', 'name', 'created_at', 'updated_at']
            ]);
    }

    public function test_store(): void
    {
        Passport::actingAs(User::factory()->create());

        $response = $this->postJson('/api/tags', [
            'name' => 'New Tag'
        ]);

        $response->assertStatus(201)
            ->assertJsonFragment(['name' => 'New Tag']);
        $this->assertDatabaseHas('tags', ['name' => 'New Tag']);
    }

    public function test_update(): void
    {
        Passport::actingAs(User::factory()->create());

        $tag = Tag::factory()->create();

        $response = $this->putJson("/api/tags/{$tag->id}", [
            'name' => 'Updated Tag'
        ]);

        $response->assertStatus(200)
            ->assertJsonFragment(['name' => 'Updated Tag']);
        $this->assertDatabaseHas('tags', ['name' => 'Updated Tag']);
    }

    public function test_destroy(): void
    {
        Passport::actingAs(User::factory()->create());

        $tag = Tag::factory()->create();

        $response = $this->deleteJson("/api/tags/{$tag->id}");

        $response->assertStatus(204);
        $this->assertDatabaseMissing('tags', ['id' => $tag->id]);
    }
}
